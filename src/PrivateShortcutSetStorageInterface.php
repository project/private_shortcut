<?php

namespace Drupal\private_shortcut;

use Drupal\shortcut\ShortcutSetStorageInterface;

/**
 * Overrides the shortcut_set entity storage.
 */
interface PrivateShortcutSetStorageInterface extends ShortcutSetStorageInterface {

  /**
   * Gets the user account for a given private shortcut set ID.
   *
   * @param string $id
   *   The private shortcut set ID.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The user account, NULL if the user account does not exist in the system.
   *
   * @throws \InvalidArgumentException
   *   If the given ID is not a valid private shortcut set ID.
   */
  public function getAccountFromPrivateSetId($id);

  /**
   * Composes the private shortcut set ID for a given user ID.
   *
   * @param int $uid
   *   The User ID.
   *
   * @return string
   *   The private shortcut set ID.
   */
  public static function getPrivateSetIdFromUid($uid);

  /**
   * Extracts the user ID from a private shortcut set ID.
   *
   * @param string $id
   *   The private shortcut set ID.
   *
   * @return int|false
   *   The user ID, FALSE if the ID is not a valid private shortcut set ID.
   */
  public static function getUidFromPrivateSetId($id);

  /**
   * Unassign private shortcut sets that may be assigned to any user.
   *
   * @return bool
   *   TRUE if there were private shortcut sets assigned and have been
   *   successfully unassigned. FALSE if there were no private shortcut sets
   *   assigned to any user.
   */
  public function unassingPrivateSets();

}
