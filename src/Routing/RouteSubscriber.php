<?php

namespace Drupal\private_shortcut\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for private_shortcut routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Deny access to admin operations on private sets.
    $route_names = [
      'entity.shortcut_set.edit_form',
      'entity.shortcut_set.delete_form',
    ];
    foreach ($route_names as $route_name) {
      $route = $collection->get($route_name);
      if ($route) {
        $route->setRequirement('_private_shortcut_exclude', 'TRUE');
      }
    }

    // Limit some private sets operations to its owner.
    $route_names = [
      'entity.shortcut_set.customize_form',
      'shortcut.link_add',
    ];
    foreach ($route_names as $route_name) {
      $route = $collection->get($route_name);
      if ($route) {
        $route->setRequirement('_private_shortcut_editable', 'TRUE');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];

    return $events;
  }

}
