<?php

namespace Drupal\private_shortcut\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\shortcut\ShortcutSetInterface;
use Drupal\private_shortcut\PrivateShortcutSetStorage;

/**
 * Prevent access to private shortcut set edition.
 *
 * @ingroup private_shortcut_access
 */
class PrivateShortcutExcludeAccess implements AccessInterface {

  /**
   * Prevent access to private shortcut set.
   *
   * @param \Drupal\shortcut\ShortcutSetInterface $shortcut_set
   *   The shortcut set that is being accessed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(ShortcutSetInterface $shortcut_set) {
    if (PrivateShortcutSetStorage::getUidFromPrivateSetId($shortcut_set->id()) !== FALSE) {
      return AccessResult::forbidden('This operation is not available for private shortcut sets.');
    }
    else {
      return AccessResult::allowed();
    }
  }

}
