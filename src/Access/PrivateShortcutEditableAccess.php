<?php

namespace Drupal\private_shortcut\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\shortcut\ShortcutSetInterface;
use Drupal\private_shortcut\PrivateShortcutSetStorage;

/**
 * Restrict private shortcut set access to its owner.
 *
 * @ingroup private_shortcut_access
 */
class PrivateShortcutEditableAccess implements AccessInterface {

  /**
   * Prevent access to non-owned private shortcut set.
   *
   * @param \Drupal\shortcut\ShortcutSetInterface $shortcut_set
   *   The shortcut set that is being accessed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(ShortcutSetInterface $shortcut_set) {
    $uid = PrivateShortcutSetStorage::getUidFromPrivateSetId($shortcut_set->id());
    if ($uid !== FALSE && $uid != \Drupal::currentUser()->id()) {
      return AccessResult::forbidden('Only its owner can edit private shortcut sets.');
    }
    else {
      return AccessResult::allowed();
    }
  }

}
