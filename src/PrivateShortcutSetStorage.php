<?php

namespace Drupal\private_shortcut;

use Drupal\Core\Session\AccountInterface;
use Drupal\shortcut\ShortcutSetStorage;
use Drupal\user\Entity\User;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides the shortcut_set entity storage.
 */
class PrivateShortcutSetStorage extends ShortcutSetStorage implements PrivateShortcutSetStorageInterface {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a PrivateShortcutSetStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_info
   *   The entity info for the entity type.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeInterface $entity_info, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, MemoryCacheInterface $memory_cache, Connection $connection, AccountInterface $current_user) {
    parent::__construct($entity_info, $config_factory, $uuid_service, $module_handler, $language_manager, $memory_cache, $connection);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_info) {
    return new static(
      $entity_info,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('module_handler'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('database'),
      $container->get('current_user')
    );
  }

  /**
   * Creates the private shortcut set for a given user account.
   *
   * Private shortcut set config entities are dinamically created on demand
   * and never get saved.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user account for the private shortcut set. The current
   *   user if no one is given.
   *
   * @return \Drupal\shortcut\ShortcutSetInterface
   *   The user's private shortcut set.
   */
  protected function userPrivateSet(AccountInterface $account = NULL) {
    if (!$account) {
      $account = $this->currentUser;
    }

    $set_id = static::getPrivateSetIdFromUid($account->id());
    $cached = $this->memoryCache->get($this->buildCacheId($set_id));
    if ($cached) {
      $shortcut_set = $cached->data;
    }
    else {
      $shortcut_set = $this->create([
        'id' => $set_id,
        'label' => $account->id() == $this->currentUser->id()
          ? $this->t('Private')
          : $this->t("User's private"),
      ]);

      $this->memoryCache->set($this->buildCacheId($set_id), $shortcut_set, MemoryCacheInterface::CACHE_PERMANENT, [$this->memoryCacheTag]);
    }

    return $shortcut_set;
  }

  /**
   * {@inheritdoc}
   */
  public function doLoadMultiple(array $ids = NULL) {
    $shortcut_sets = [];
    if ($ids === NULL) {
      // Add current user private shortcut set when loading all available sets.
      $current_user_set = $this->userPrivateSet();
      $shortcut_sets[$current_user_set->id()] = $current_user_set;
    }
    else {
      // Add private user sets found on requested IDs.
      $final_ids = [];
      foreach ($ids as $id) {
        try {
          if ($account = $this->getAccountFromPrivateSetId($id)) {
            // If the user account of a private shortcut set doesn't exist
            // their private set will also not be present.
            $shortcut_sets[$id] = $this->userPrivateSet($account);
          }
        }

        catch (\InvalidArgumentException $e) {
          // Not a private shortcut set ID.
          $final_ids[] = $id;
        }
      }

      $ids = $final_ids;
    }

    return $shortcut_sets + parent::doLoadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccountFromPrivateSetId($id) {
    $uid = static::getUidFromPrivateSetId($id);
    if ($uid === FALSE) {
      throw new \InvalidArgumentException($id . ' is not a valid private shortcut set ID.');
    }

    return $uid ? User::load($uid) : User::getAnonymousUser();
  }

  /**
   * {@inheritdoc}
   */
  public static function getPrivateSetIdFromUid($uid) {
    return '~' . $uid;
  }

  /**
   * {@inheritdoc}
   */
  public static function getUidFromPrivateSetId($id) {
    $id_parts = explode('~', $id);
    if ($id_parts[0] == '' && isset($id_parts[1]) && is_numeric($id_parts[1])) {
      return intval($id_parts[1]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCacheId($id) {
    if (static::getUidFromPrivateSetId($id) !== FALSE) {
      // Private shortcut set config entities cannot be overrided.
      return "values:{$this->entityTypeId}:$id";
    }

    return parent::buildCacheId($id);
  }

  /**
   * {@inheritdoc}
   */
  protected function doPreSave(EntityInterface $entity) {
    if (static::getUidFromPrivateSetId($entity->id()) !== FALSE) {
      throw new EntityStorageException('Private storage set cannot be saved.');
    }

    parent::doPreSave($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function unassingPrivateSets() {
    $deleted = $this->connection->delete('shortcut_set_users')
      ->condition('set_name', '~%', 'LIKE')
      ->execute();
    return (bool) $deleted;
  }

}
