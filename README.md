# Private Shortcut Drupal module

## Contents of this file

* Summary
* Requirements
* Installation
* Configuration
* Usage
* Contact

## Summary

Allows users to have their own private shortcuts. This module extends the core
shortcut module by providing a dynamically created shortcut set for each user
that is never saved nor added to the exported configuration.

## Requirements

The module depends on the Drupal core shortcut and user modules.

## Installation

Install as usual, see https://www.drupal.org/node/1897420 for further
information. You might rebuild the cache after (un)installation.

## Configuration

The Private Shortcut module doesn't require any particular configuration.

## Usage

Once the module is installed, the default shortcut set will be replaced by
the user's private unless:

* the user has a shortcut set explicity assigned
* the user has no permission to customize shortcut links

## Contact

Current maintainers:
* Manuel Adan (manuel.adan) - https://www.drupal.org/user/516420
